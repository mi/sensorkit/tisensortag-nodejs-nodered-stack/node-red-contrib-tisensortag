# Changelog

## 0.2.3
- reenabling Web API scanning and status of SensorTags
/tisensortag/scanresult
/tisensortag/sensortags

- reenabled initial battery read but with delay  to not conflict with setup all sensors

## 0.2.2
- further bugfixing handling connections, i.e. disconnections not triggered by central

## 0.2.1
- smaller bugfixing handling disconnected, rediscovered and reconnected devices
- common timestamp for IMU data (accelero-, gyro-, magnetometer), notify call adjusted, needs now tisensortag lib >= 0.0.6

## 0.2.0
- BIG code cleanup and bugfixing
- much clearer code for node and manager

###Device Configuration
- removed configMode and special config file for SensorTag assignment
- deviceId and deviceNo are now stored in flow-credentials for easy edit outside of flows-file
- up to eight SensorTags, with unique number between 1 and 8

## 0.1.17
- frontend: threshold and latency reenabled for settings for SensorTag firmware >1.45
- frontend: deviceID field checks pattern of mac address and strips out delimiters
- backend: bugfix for forgotten barometerLatency

## 0.1.16
- bug in thresholding and latency preventing correct parameter settings with exception of luxmeter
- disabled according threshold and latency fields in GUI
- disabled movement sensor config fields if movement is disabled

## 0.1.15
bugfixes
- G-range limited to 8G --> value calculation in sensortag lib is restricted to 8G range (hardcoded at the moment)
- connect / disconnect status indication mismatch fixed
- code cleaning

## 0.1.14
- non-backward compatible version update (0.0.x --> 0.1.x)
- new config format
- new output format
- libMac no longer needed
- one SensorTag per Node
- manual SensorTag-ID config possible (no scan)

BUGS
- multi msg emission for SensorTag #1 (the first SensorTag Node)

# 0.0.13
- stopScanningAndStartConnecting timeout raised from 5s to 12s to have more time to find the SensorTags
- bugfixing in edit form: saving selected devices in config mode "node" works again
- decreased log level

# 0.0.12
- bugfixing in edit form (WOM timeout) and internally
- refactoring WOS --> WOM to make it more clear that it is any motion and not only shaking that activates the movement sensor

# 0.0.11
- new: configMode and configFile that allow to read the node configuration from outside the node (e.g. from a json file)

# 0.0.10
- included third address range of SensorTags MAC addresses in filter

# 0.0.9
- WOM threshold mapping adjusted for 1.44 firmware, now covering the whole possible range from 0 mG up to 1020 mG in meaningly distributed steps

# 0.0.8
- "Rescan" now triggers updatescanning (scan without disconnecting the connected devices)
- RSSI updates: after each reception of a BLE frame the RSSI value is updated and sent as msg.rssi meta data

# 0.0.7
- config: list of known SensorTags changed in file location and file format
- MOV new feature: G-Range configurable (node-tisensortag 0.0.2 and firmware 1.42 needed)

# 0.0.6
- bug fix and new validation function for inactivity timeout for movement sensor (10s is now valid)
- already configured SensorTags are now checked in the list of devices

# 0.0.5
- new series of SensorTag with MAC addresses starting with 247189 are now visible in scans

# 0.0.4
- internal changes in management of SensorTags
- additional information to SensorTags: lastUpdate is kept up to date with every read out / notify from a connected device or on every discovery for an advertising device
- additional information to SensorTags: status with values: advertising, connected and disconnected
- additional information to SensorTags: batteryLevel with last updated value (for connected devices only)
- new Web-API hook: /tisensortag/updatescanning triggers a new scanning for some seconds before it is automatically been stopped again

# 0.0.3
- additional information to SensorTags: Status (RSSI) and name (for known devices) via a local JSON config file

# 0.0.2
- scrollbar in edit dialog
- only SensorTags will show up on scans (with MAC addresses starting with a0e6f8)

# 0.0.1
- see changes below