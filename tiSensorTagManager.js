const loglevel = 2;
const BLE_SCAN_TIMEOUT = 8000;
const BLE_SCAN_RESCAN_TIMEOUT = 20000;
const BLE_CONNECT_TIMEOUT = 16000; // ~5s per device
const MANAGE_LOOP_INTERVAL = 6000;
const MAX_NUMBER_OF_SENSORTAGS = 5;

var TiSensorTag = require("tisensortag"); // the modified version needed!

var manageLoop;

var sensorTags = {}; // TI SensorTag devices representation (and as such nodes) 
var nobleDevicesAdvertising = {}; // the BLE devices (SensorTags?) that are found advertising during the last scan
//var sensorTagNodes = {}; //nodes in Node-Red flow, instances of tiSensorTagNode  //deprecated

var isScanning = false;
var isConnecting = false;
var isSafeToScan = true;
var isSafeToConnect = false;

var initScan = false;
var hasScanned = false;
let lastScanTime = 0;
let scanResults = [];

var log;
if (loglevel >= 1) {
    log = function (msg) { console.log("[TiSensorTag Manager] " + msg); };
} else {
    log = function () { };
}

var logWarn;
if (loglevel >= 2) {
    logWarn = function (msg) { console.log("[TiSensorTag Manager] " + msg); };
} else {
    logWarn = function () { };
}

var logInfo;
if (loglevel >= 3) {
    logInfo = function (msg) { console.log("[TiSensorTag Manager] " + msg); };
} else {
    logInfo = function () { };
}

var logDebug;
if (loglevel >= 4) {
    logDebug = function (msg) { console.log("[TiSensorTag Manager] " + msg); };
} else {
    logDebug = function () { };
}


function init() {
	setTimeout ( function () {let mangageLoop = setInterval(manageSensorTags, MANAGE_LOOP_INTERVAL);}, 500); // a small start delay to let all nodes register before main loop starts    
}

function manageSensorTags() {
	if (!isScanning && !isConnecting) {
		for (var deviceId in sensorTags) {	
			if (sensorTags[ deviceId ].isAdvertising == true) { //some device is advertising and ready to connect
				startConnecting();
				break;
			}
		}
		
		if (initScan) {
			startScanning();
			return;
		}
			
		for (var deviceId in sensorTags) { 
			if (sensorTags[ deviceId ].isConnected == false) { //some device is not connected and need to be found
				startScanning();
				break;
			}
		}
	}
}  

function addNode(node) {
	/*sensorTagNodes[node.id] = {
		node: node
	};*/ //deprecated
	if (sensorTags.hasOwnProperty(node.deviceId)) {
		logWarn("deviceId " + node.deviceId + " already configured in other node");
		node.updateStatus( "red" , "deviceId " + node.deviceId + " already configured in other node" );
	}
	else {
		for (var i in sensorTags) {
			if (sensorTags[ i ].deviceNo == node.deviceNo) {
				logWarn("deviceNo " + node.deviceNo + " already configured in other node");
				node.updateStatus( "red" , "deviceNo " + node.deviceNo + " already configured in other node" );
				return;
			}
		}
		sensorTags[node.deviceId] = node;
	}
}

function removeNode(node, removed, done) {
    /*if( sensorTags[ node.deviceId ].isConnected ) {
		sensorTags[ node.deviceId ].isConfigured = false;
	}*/
	//delete sensorTagNodes[node.deviceId]; //deprecated
	delete sensorTags[node.deviceId];

	if (removed) {
	}
	else {
		// Most likely a re-deploy
	}
	
	done();
}


// is called when SensorTags are configured but not connected
// or on-demand to find other BLE devices / new SensorTags
function startScanning() { 
    logDebug("startScanning");
	if (isScanning) return;
	if (!isSafeToScan) return;
	initScan=false;
	lastScanTime=(new Date()).getTime();
    nobleDevicesAdvertising = {};
	hasScanned = false;
    isScanning = true;
    isSafeToConnect = false;	
    TiSensorTag.discoverAll(onDiscover);
    setTimeout(stopScanning, BLE_SCAN_TIMEOUT);
}

function stopScanning() {
	logDebug("stopScanning");
	if (!isScanning) return;
    TiSensorTag.stopDiscoverAll(onDiscover);
    isScanning = false;
    isSafeToConnect = true;
	hasScanned = true;
	
	scanResults = [];
    for (var id in nobleDevicesAdvertising) {
		var nobleDevice = nobleDevicesAdvertising[id];
		var scanResult = {};
		scanResult.id = nobleDevice.id;
		if (sensorTags[id]) {		
			scanResult.name = sensorTags[id].name;
		}
		else {
			scanResult.name = "unknown " + nobleDevice._peripheral.advertisement.localName;
		}
		scanResult.rssi = nobleDevice._peripheral.rssi;
		scanResult.updated =  nobleDevice.updated;
        scanResults.push(scanResult);
    }
}

function startConnecting() {
	logDebug("startConnecting");
	if (isConnecting) return;
	if (!isSafeToConnect) return;
	isSafeToScan = false;
	isConnecting = true;
	setTimeout(stopConnecting, BLE_CONNECT_TIMEOUT);
	for (var deviceId in sensorTags) {
		if (sensorTags[ deviceId ].isAdvertising) {
			sensorTags[ deviceId ].connect();
		}
	}    
}

function stopConnecting() {
	logDebug("stopConnecting");
	isConnecting = false;
	isSafeToScan = true;
}

function onDiscover(nobleDevice) { // nobleDevice is a tiSensorTag device, an extended noble-device instance
    nobleDevicesAdvertising[nobleDevice.id] = nobleDevice;
    nobleDevicesAdvertising[nobleDevice.id].updated = (new Date()).getTime();

    if (sensorTags.hasOwnProperty(nobleDevice.id)) { // a configured SensorTag		
		sensorTags[nobleDevice.id].onDiscover(nobleDevice);
    }
	else { // another BLE device, maybe a SensorTag that is not configured
		if(nobleDevice._peripheral.advertisement.localName === "CC2650 SensorTag") {
			logInfo("discovered " + nobleDevice.id + " CC2650 SensorTag " + nobleDevice._peripheral.rssi + "dB");
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
// web API functions

function getSensorTags() {
    let sensorTagsStatus = {};
	for (let deviceId in sensorTags) {
		let sensorTag = sensorTags[deviceId];
		let sensorTagStatus  = {};
		
		sensorTagStatus.deviceId = sensorTag.deviceId;
		sensorTagStatus.deviceNo = sensorTag.deviceNo;
		sensorTagStatus.name = sensorTag.name;
		sensorTagStatus.updated = sensorTag.updated;
		sensorTagStatus.rssi = sensorTag.twin.rssi;
		sensorTagStatus.batteryVoltage = sensorTag.twin.battery.voltage;
		
		sensorTagStatus.status = "disconnected";	
		if (sensorTag.isAdvertising == true) { //some device is advertising and ready to connect
			sensorTagStatus.status = "advertising";		
		}
		if (sensorTag.isConnected == true) { //some device is advertising and ready to connect
			sensorTagStatus.status = "connected";		
		}
		
		sensorTagsStatus[sensorTagStatus.deviceNo] = sensorTagStatus;
	}
	
    return sensorTagsStatus;
}

function triggerScan() {
	if (!isScanning) {
		initScan = true;
	}
}	

function getScanResult() {
    if( ( (new Date()).getTime() - lastScanTime ) > BLE_SCAN_RESCAN_TIMEOUT ) {
		triggerScan();
	//}
	//if( ( (new Date()).getTime() - lastScanTime ) > 2*BLE_SCAN_RESCAN_TIMEOUT ) {
		//if it is just too old, display only a placeholder message
		scanResults = [];
		let scanResult = {
			id: "0",
			name: "scan just triggered",
			rssi: 0,
			updated: (new Date()).getTime()
		}
		scanResults.push(scanResult);
	}
	
	return scanResults;
}

function getIsScanning() { return isScanning; }

module.exports = {
    init: init,
    addNode: addNode,
    removeNode: removeNode,
    getSensorTags: getSensorTags,
	getScanResult: getScanResult,
    triggerScan: triggerScan
};
