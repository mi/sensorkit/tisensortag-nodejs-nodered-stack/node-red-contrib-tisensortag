function clamp( v , min , max ) {
  return ( v < min ? min : ( v > max ? max : v ) );
}

module.exports = function( RED ) {
  var Manager = require( "./tiSensorTagManager.js" ); // using the tisensortag library
  Manager.init();

  var enableLogging = true;

  function TiSensorTagNode( config ) {
    // config is loaded from the flow file (configs of instances of tisensortag)
	RED.nodes.createNode( this , config );
    var self = this;

	//device specific config
    //this.deviceId = config.deviceId;  //the ble mac address to connect to (globally unique)
    //this.deviceNo = config.deviceNo; //arbitrary number to distinguish devices (locally unique)
	
	for( var key in config )
      this[ key ] = config[ key ];
		
	this.deviceId = this.credentials.deviceId;  //the ble mac address to connect to (globally unique)
    this.deviceNo = this.credentials.deviceNo; //arbitrary number to distinguish devices (locally unique)
	
    this.name = config.name; //arbitrary local name
    this.firmware = config.firmware; //defines what services and features are possible
	
    //service specific config    
	if (this.firmware < "1.30") { // battery service was included beginning with firmware 1.30	  
      this.batteryEnable = false;	  
	  this.batteryNotify = false;
    } else {
	  this.batteryEnable = config.batteryEnable;
	  this.batteryNotify = config.batteryNotify;
    }

    this.config = config;
	this.twin = {};
	this.twin.battery = {};
	this.twin.battery.voltage = 0;
	this.twin.battery.updated = 0;
	this.twin.rssi = 0;
	
	//connection and status specific
	this.nobleDevice;
	this.isConfigured = true;
	this.isAdvertising = false;
	this.isConnected = false;
	this.isReconnect = false;
	this.updated = 0;
	
    this.on( 'close' , function( removed, done ) {
	  Manager.removeNode( self , removed, done );
      if (this.isConnected) { 
		this.disconnect();
	  }
    } );
	
    if( (this.deviceId != "") && (this.deviceNo != "") ) {
		this.updateStatus( "yellow" , "configured " + this.deviceId);
		Manager.addNode( self );
		
	} else {
		this.updateStatus( "red" , "not all required fields configured" );
	}
  }

  RED.nodes.registerType( "tiSensorTag" , TiSensorTagNode,{
       credentials: {
         deviceNo: {type:"text"},
         deviceId: {type:"text"}
	   }
     } );
  
  TiSensorTagNode.prototype.sendData = function( sensor , data ) {
    var now = ( new Date() ).getTime();
	this.updated = now;
	this.nobleDevice._peripheral.updateRssi();
	
    this.twin[sensor] =  data;
	this.twin[sensor].updated =  now;
	
	this.twin.rssi = this.nobleDevice._peripheral.rssi;
	
    this.send( {
	  deviceId: this.deviceId,
	  deviceNo: this.deviceNo,
	  deviceName: this.name,
      sensor: sensor,
	  ts: now,
      rssi: this.nobleDevice._peripheral.rssi,
      payload: data
    } );
  };
  
   TiSensorTagNode.prototype.sendDataTs = function( sensor , data, timestamp ) {
	this.updated = timestamp;
	this.nobleDevice._peripheral.updateRssi();
    this.send( {
	  deviceId: this.deviceId,
	  deviceNo: this.deviceNo,
	  deviceName: this.name,
      sensor: sensor,
	  ts: timestamp,
      rssi: this.nobleDevice._peripheral.rssi,
      payload: data
    } );
  };
  

  TiSensorTagNode.prototype.updateStatus = function( color , message ) {
    this.status( {
      fill : color,
      shape : "dot",
      text : message
    } );
  };
  

  TiSensorTagNode.prototype.log = function( msg ) {
    if( enableLogging ) {
      console.log( "[TiSensorTag Node] " + msg );
      //this.log( "[" + this.tag.uuid + "] " + msg );
    } else {
      //
    }
  };
  
  TiSensorTagNode.prototype.onDiscover = function( nobleDevice ) {
	this.log("discovered " + nobleDevice.id + " " + this.name + " " + nobleDevice._peripheral.rssi + " dB");
	this.updateStatus( "blue" , "discovered " + nobleDevice.id );
	this.nobleDevice = nobleDevice;
	
	if (this.isConnected) { //should already be connected but is obviously not ... --> How to go on? easiest: wait for disconnect event from stack
		//this.isReconnect = true;
		//this.isAdvertising = false;  // do wait with further steps to connect until the peripheral is disconnected by stack timeout
		this.disconnect(); // //force a disconnect first to reconnect afterwards
	}
	else {
		this.isAdvertising = true;	
	}
	
	//this.isConnected = false; //maybe set disconnect only when stack calls onDisconnect
	
	return true;  
  }; 
  
	TiSensorTagNode.prototype.connect = function() {
		//if (this.isReconnect == false) {
			this.log("connect " + this.deviceId + " " + this.name);
			this.nobleDevice.connect(this.onConnect.bind( this ));
		//}
		//else {
			//this.disconnect(); // //force a disconnect first to reconnect afterwards
		//}
		this.isAdvertising = false;
	};
	
	TiSensorTagNode.prototype.disconnect = function() {
		this.updateStatus( "blue" , "disconnect " + this.deviceId );
		this.log("disconnect " + this.deviceId + " " + this.name);
		this.nobleDevice.disconnect();
	};
	
	TiSensorTagNode.prototype.onConnect = function(error) {
		if (error) {
			this.updateStatus( "red" , error );
			this.log( "Error onConnect: " + error.message );
			this.isConnected = false;
		}
		else {
			this.isConnected = true;
			this.updateStatus( "green" , "connected " + this.deviceId );
			this.log("connected " + this.deviceId + " " + this.name );
			
			this.nobleDevice.on( "disconnect" , this.onDisconnect.bind( this ) );
			this.nobleDevice.discoverServicesAndCharacteristics( this.discoverServCharCallback.bind( this ) );
			setTimeout ( this.updateConnectionParameters.bind( this ), 12000);
		}
	};
	
	TiSensorTagNode.prototype.updateConnectionParameters = function()  {
		const { exec, spawn } = require('child_process');
		
		exec('hcitool con', (error, stdout, stderr) => {
		  if (error) {
			this.log(`error: ${error.message}`);
			return;
		  }

		  if (stderr) {
			this.log(`stderr: ${stderr}`);
			return;
		  }
		  
		  var lines = stdout.split("\n");
		  for (var line in lines) {
			  var tokens = lines[line].split(" ");
			  var deviceId = new String(tokens[2]).replace(/:/g, "").toLowerCase();
			  var handle = tokens[4];
			  if (deviceId === this.deviceId) {
			    //this.log("deviceId " + deviceId + " handle " + handle);
				var child = spawn('hcitool', ['lecup', '--handle='+handle, '--min=0x0028', '--max=0x0050', '--latency=0x0064', '--timeout=0x0960']); //timeout=0x0c80
				//var child = spawn('hcitool', ['lecup', '--handle='+handle, '--min=0x0040', '--max=0x0080', '--latency=0x0062', '--timeout=0x0c80']); //accepted but too slow for fast movement sampling
				//var child = spawn('hcitool', ['lecup', '--handle=64', '--min=40', '--max=40', '--latency=10', '--timeout=1000']); //these parameters are accepted _sometimes_
					child.stdout.on('data', data => {
					  console.log(`stdout:\n${data}`);
					});
					child.stderr.on('data', data => {
					  console.error(`stderr: ${data}`);
					});
				break;
			  }
		  }
		});
	};
	
    TiSensorTagNode.prototype.onDisconnect = function()  {
      this.isConnected = false;
	  this.isReconnect = false;
	  this.updateStatus( "red" , "disconnected " + this.deviceId );
	  this.log("disconnected " + this.deviceId + " " + this.name );		
    };
	
  TiSensorTagNode.prototype.discoverServCharCallback = function( error )  {
    if( error )
    {
      this.log( "Error getting services & characteristics: " + error.message );
      return;
    }

    if( this.isConnected == false ) return;

    if( this.batteryEnable ) {
      setTimeout( () => this.nobleDevice.readBattery(), 10000);  //might cause errors if called too early
      if ( !this.isReconnect ) this.nobleDevice.on( "batteryChange" , this.onBatteryChange.bind( this ) );
      this.nobleDevice.notifyBattery( this.errorHandler.bind( this ) );
    }

    if( this.keysEnable ) {
      if ( !this.isReconnect ) this.nobleDevice.on( "simpleKeyChange" , this.onKeyChange.bind( this ) );
      this.nobleDevice.notifySimpleKey( this.errorHandler.bind( this ) );
    }

    if( this.barometerEnable ) {
      this.nobleDevice.enableService( "barometer", this.barometerPeriod*1000 , parseInt(this.barometerLatency), parseInt(this.barometerThreshold), this.errorHandler.bind( this ) );
      if ( !this.isReconnect ) this.nobleDevice.on( "barometerChange" , this.onBarometerChange.bind( this ) );
      this.nobleDevice.notifyBarometer( this.errorHandler.bind( this ) );
    }

    if( this.hygrometerEnable ) {
      this.nobleDevice.enableService( "hygrometer", this.hygrometerPeriod*1000 , parseInt(this.hygrometerLatency), parseInt(this.hygrometerThreshold), this.errorHandler.bind( this ) );
      if ( !this.isReconnect ) this.nobleDevice.on( "hygrometerChange" , this.onHygrometerChange.bind( this ) );
      this.nobleDevice.notifyHygrometer( this.errorHandler.bind( this ) );
    }

    if( this.luxmeterEnable ) {
      this.nobleDevice.enableService( "luxmeter", this.luxmeterPeriod*1000 , parseInt(this.luxmeterLatency), parseInt(this.luxmeterThreshold), this.errorHandler.bind( this ) );
      if ( !this.isReconnect ) this.nobleDevice.on( "luxmeterChange" , this.onLuxmeterChange.bind( this ) );
      this.nobleDevice.notifyLuxmeter( this.errorHandler.bind( this ) );
    }

    if( this.thermometerEnable ) {
      this.nobleDevice.enableService( "thermometer", this.thermometerPeriod*1000 , parseInt(this.thermometerLatency), parseInt(this.thermometerThreshold), this.errorHandler.bind( this ) );
      if ( !this.isReconnect ) this.nobleDevice.on( "thermometerChange" , this.onThermometerChange.bind( this ) );
      this.nobleDevice.notifyThermometer( this.errorHandler.bind( this ) );
    }

    if( this.movementEnable ) {      
      if( this.movementAccelerometerEnable ) {
        this.nobleDevice.enableAccelerometer();
        if ( !this.isReconnect ) this.nobleDevice.on( "accelerometerChange" , this.onAccelerometerChange.bind( this ) );

        this.nobleDevice.setMovementGRange( parseInt(this.movementAccelerometerGRange), this.errorHandler.bind( this ) );

        if( this.movementAccelerometerWomEnable ) {
          this.nobleDevice.setMovementWomEnable( this.errorHandler.bind( this ) );
          this.nobleDevice.setMovementWomThreshold(parseInt(this.movementAccelerometerWomThreshold) , this.errorHandler.bind( this ) );
          this.nobleDevice.setMovementWomTimeout(parseInt(this.movementAccelerometerWomTimeout) , this.errorHandler.bind( this ) );
        }
      }

      if( this.movementGyrometerEnable ) {
        this.nobleDevice.enableGyrometer();
        if ( !this.isReconnect ) this.nobleDevice.on( "gyrometerChange" , this.onGyrometerChange.bind( this ) );
      }

      if( this.movementMagnetometerEnable ) {
        this.nobleDevice.enableMagnetometer();
        if ( !this.isReconnect ) this.nobleDevice.on( "magnetometerChange" , this.onMagnetometerChange.bind( this ) );
      }

      this.nobleDevice.enableMovement( this.errorHandler.bind( this ) );
	  this.nobleDevice.setMovementPeriod( this.movementPeriod*1000 , this.errorHandler.bind( this ) );
      this.nobleDevice.notifyMovement( this.errorHandler.bind( this ) );
    }
  };

  TiSensorTagNode.prototype.onBatteryChange = function( level )  {
    this.sendData( "battery" , {
      voltage : Math.round(level * 3273 / 100)
    } );
  };

  TiSensorTagNode.prototype.onKeyChange = function( left , right, reedRelay )  {
    this.sendData( "keys" , {
      key1 : left,
      key2 : right,
      reed : reedRelay
    } );
  };

  TiSensorTagNode.prototype.onBarometerChange = function( pressure, temperature )  {
    this.sendData( "barometer" , {
      pressure : pressure,
      temperature : temperature
    } );
  };

  TiSensorTagNode.prototype.onHygrometerChange = function( temperature , humidity )  {
    this.sendData( "hygrometer" , {
      temperature : temperature,
      humidity : humidity
    } );
  };

  TiSensorTagNode.prototype.onLuxmeterChange = function( illuminance )  {
    this.sendData( "luxmeter" , {
      illuminance : illuminance
    } );
  };

  TiSensorTagNode.prototype.onThermometerChange = function( object , ambient )  {
    this.sendData( "thermometer" , {
      objectTemperature : object,
      ambientTemperature : ambient
    } );
  };

  TiSensorTagNode.prototype.onAccelerometerChange = function( x , y , z, ts )  {
    this.sendDataTs( "accelerometer" , {x : x, y : y, z : z}, ts);
  };

  TiSensorTagNode.prototype.onGyrometerChange = function( x , y , z, ts )  {
    this.sendDataTs( "gyrometer" , {x : x, y : y, z : z}, ts);
  };

  TiSensorTagNode.prototype.onMagnetometerChange = function( x , y , z, ts )  {
    this.sendDataTs( "magnetometer" , {x : x, y : y, z : z}, ts);
  };

  TiSensorTagNode.prototype.errorHandler = function( error )  {
    if( error )
    {
      this.log( "Error: " + error.message );
    }
  };

// Web API

  RED.httpNode.get( "/tisensortag/sensortags" , function( request , response ) {
    response.setHeader( "Content-Type" , "application/json" );
    response.end( JSON.stringify(Manager.getSensorTags() ) );
  } );

  // trigger a scanning
  RED.httpNode.get( "/tisensortag/triggerscan" , function( request , response ) {
    Manager.triggerScan();
    response.sendStatus( 204 );
  } );
 
  RED.httpNode.get( "/tisensortag/scanresult" , function( request , response ) {
    //response.sendStatus( 200 );
	response.setHeader( "Content-Type" , "application/json");
	response.end( JSON.stringify(Manager.getScanResult()) );
  } );
};
