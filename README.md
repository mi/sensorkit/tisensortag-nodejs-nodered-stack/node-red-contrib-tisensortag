# node-red-contrib-tisensortag
To use the [TI CC2650 SensorTag (CC2650STK)](http://www.ti.com/tool/cc2650stk) in Node-Red.

It is a based on based on (node-red-contrib-sensortag)[https://github.com/ibm-iot/node-red-contrib-sensortag].

## Prerequisites
It requires node-tisensortag lib.

It requires a firmware >1.30.

With stock firmware 1.40 it enables at least some new features (WOM, battery, reed, ...)-

For some advanced features a custom built firmware on the SensorTag (>1.40) is required.

As node-tisensortag is currently only for local install it should be installed first into node-red-contrib-tisensortag before itself is being installed int Node-Red.
Adjust the include path in the package.json file accordingly.

## Install
Understand and fulfill the prerequisites.

Currently node-red-contrib-tisensortag is not listed in the npm repository and it needs a manual install.
  * download the files into a local directory, e.g. into ~/node-red-contrib-tisensortag
  * go into the directory where node-red-contrib-tisensortag is to be installed (normally this sould be ~/.node-red
  * perform a npm install from the local source directory

```sh
npm install ~/node-red-contrib-tisensortag/
```

## General
Everything related to the SensorTag 1.0 version based on the CC2540 is stripped out for the sake of simplicity.

The config dialog has got some cleanup.

A new (custom built) Firmware version for advanced features is needed (based on 1.40 at least).

## Sensor / service handling
All sensors expect movement: extended period 100ms .. 4200s with enhanced customs firmware utilizing the CONF characteristic (FW: custom 1.42+)

### Battery
Battery level handling (FW 1.30+)

### 9-DOF Movement Sensor
only one period for all values possible (only one BLE characteristic for only one sensor)

new
Wake-on-Motion/Shake (WOM) can be configured (FW 1.20+)
WOM-THRESHOLD config (FW: custom)
INACTIVITY TIMEOUT config (FW: custom)

changed
xA, yA, zA for accelerometer values
xM, yM, zM for magnetometer values
xG, yG, zG for gyroscope values

TBD: auto retrigger for motion detection while not being idle
TBD: G-Range config (FW: 1.30+)

### Keys
status of reed switch is forwarded

### Barometer
temperature is forwarded